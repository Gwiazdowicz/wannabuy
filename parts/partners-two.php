<div class="partners partners--two l-borders">
    <div class="l-container">
        <h2 class="partners__title">Zaufali nam</h2>
        <div data-slider="logosOne" class="js-init-slider partners__slider">
            <?php for ($i=1; $i < 9; $i++) : ?>
                <div class="partners__slider-slide">
                    <img class="partners__slider-item" src="<?= IMG . 'slider-2.'. $i .'.png' ?>" alt="">
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>
