<section class="about">
        <div class="about__border borders">
            <div class="about__container container">
                <div class="about__box --observe appear-up">
                    <div class="about__content">
                        <h2 class="about__title-top-two a-title-two a-title-two--red">Nie zmarnuj tej szansy!</h2>
                        <h2 class="about__title a-title-two a-title-two--red">Na tej stronie konsultacja jest darmowa!</h2>
                    </div>
                    <div class="about__content-big">
                        <h2 class="about__title a-title-two a-title-two--red">Nie zmarnuj tej szansy! Na tej stronie konsultacja jest darmowa! </h2>
                    </div>
                    <div class="about__content-bottom">
                        <div class="about__box-one">
                            <p class="a-article with-arrows arrow-left">Normalnie koszt konstultacji to 1000 zł - jeśli skorzystasz z&nbsp;naszej oferty poprzez tę stronę - jest ona darmowa!</p>
                            <p class="a-article"><b>UWAGA:</b> Zanim zdecydujesz się na darmową konsultację kadrową chcemy żebyś wiedział, że istnieje ona tylko po to by wspierać ludzi bardzo mocno zdeterminowanych na poprawę sytuacji kadrowej w&nbsp;swojej firmie. Zrobimy co w&nbsp;naszej mocy, ale wdrożenie i&nbsp;realizacja tego planu będzie wymagała zaangażowania Twojej firmy.</p>
                            <p class="a-article">Jeżeli jesteś gotowy zmienić sytuację kadrową na lepsze, umów swoją konsultację już teraz.</p>
                            <p class="a-article with-arrows arrow-right">Od czasu kiedy uruchomiliśmy ten landing page nasz kalendarz zaczyna pękać w&nbsp;szwach, a&nbsp;dostępne terminy analityków nikną w&nbsp;oczach - mamy ograniczoną ilość miejsc i&nbsp;nie wiemy jak długo będziemy w&nbsp;stanie oferować darmową konsultację. Zdecyduj się teraz zanim znowu zaczniemy pobierać za nią pieniądze!
                            </p>
                        </div>   
                    </div>
                </div>
                <div class="about__btn">
                    <a href="https://www.wannabuy.pl/new/subpage.php"  class="about__btn-text a-btn-two">Odbierz 30 minutową darmową konsultację HR </a>
                </div>
            </div>    
        </div>    
</section>
