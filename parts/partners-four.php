<div class="partners partners--four l-borders">
    <div class="l-container">
        <div data-slider="logosOne" class="js-init-slider partners__slider">
            <?php for ($i=1; $i < 8; $i++) : ?>
                <div class="partners__slider-slide">
                    <img class="partners__slider-item" src="<?= IMG . 'slider-2.'. $i .'.png' ?>" alt="">
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>
