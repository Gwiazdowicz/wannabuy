<div class="partners partners--three l-borders">
    <div class="l-container">
        <h2 class="partners__title">Dostępne na Wannabuy</h2>
        <div data-slider="logosOne" class="js-init-slider partners__slider">

            <?php for ($i=1; $i < 15; $i++) : ?>
                <div class="partners__slider-slide">
                    <img class="partners__slider-item" src="<?= IMG . 'slider-3.'. $i .'.png' ?>" alt="">
                </div>    
            <?php endfor; ?>
            
        </div>
    </div>
</div>
