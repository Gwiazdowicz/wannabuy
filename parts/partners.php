<div class="partners l-borders">
    <div class="l-container u-pr">
        <h2 class="partners__title">Widziani w</h2>
        <div data-slider="logosOne" class="js-init-slider partners__slider">
            <?php for ($i=1; $i < 5; $i++) : ?>
                <div class="partners__slider-slide">
                    <img class="partners__slider-item" src="<?= IMG . 'slider-1.'. $i .'.png' ?>" alt="">
                </div>
            <?php endfor; ?>
        </div>        
    </div>
</div>
