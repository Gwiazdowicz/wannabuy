<section class="about">
        <div class="about__border borders">
            <div class="about__container container">
                <div class="about__box --observe appear-up">
                    <div class="about__content">
                        <h2 class="about__title-top a-title-two a-title-two--red">Tak!</h2>
                        <h2 class="about__title a-title-two a-title-two--red">Chcę konsultacji kadrowej o&nbsp;wartości 1000 zł zupełnie za darmo!</h2>
                    </div>
                    <div class="about__content-big">
                        <h2 class="about__title a-title-two a-title-two--red"> Tak! Chcę konsultacji kadrowej o&nbsp;wartości 1000 zł zupełnie za darmo!</h2>
                    </div>
                    <div class="about__content-bottom">
                        <div class="about__box-one">
                            <p class="with-arrows arrow-left a-article"><b>Jak to możliwe?</b> Chcemy zrozumieć Twój biznes, więc zadamy Ci kilka pytań odnośnie tego jak pracujecie, przeanalizujemy sytuację kadrową na rynku i&nbsp;podejście do rekrutacji oraz migracji pracowników w&nbsp;Twojej firmie i&nbsp;odniesieniu do Twojej branży - następnie w&nbsp;oparciu o&nbsp;to co nam powiesz przygotujemy rozwiązanie dopasowane specjalnie dla Ciebie… za darmo!</p>
                            <p class="a-article">Wszystko co omawiamy jest dostosowane specjalnie do Twoich potrzeb - to pewnie zabrzmi dziwnie, ale nie ma w tym żadnych „haczyków”.</p>
                            <p class="a-article">Robimy to ponieważ zazwyczaj ludzie którzy przejdą z&nbsp;nami ten proces decydują się na dalszą współpracę i&nbsp;pytają o&nbsp;możliwość pozostania naszym klientem.</p>
                            <p class="with-arrows arrow-right a-article">Jednakże prosimy żebyś miał na uwadze - to nie jest rozmowa sprzedażowa, standardowo konsultacja tego typu kosztuje u&nbsp;nas 1000 zł, będziesz rozmawiać z&nbsp;jednym z&nbsp;naszych analityków, który pomoże Ci dobrać najlepsze rozwiązanie - <b>nie oczekujemy od Ciebie, że zdecydujesz się na współpracę z&nbsp;nami.</b></p>
                        
                        </div>   
                    </div>
                </div>
                <div class="about__btn">
                    <a href="https://www.wannabuy.pl/new/subpage.php" class="about__btn-text a-btn-two">Odbierz 30 minutową darmową konsultację HR </a>
                </div>
            </div>    
        </div>    
</section>

