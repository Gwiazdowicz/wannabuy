<section class="offer">
    <div class="offer__border borders">
        <div class="offer__container container">
            <div class="offer__block">
                <div class="offer__text-box">
                    <h2 class="offer__title a-title-two --observe appear-up">Stale dbamy o&nbsp;to by pracownicy zostawali w&nbsp;Twojej firmie</h2>
                </div>
                <div class="offer__tiles-box">
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon offer__tile-icon--one"></div>
                        <p class="offer__tile-text">Zapewniamy realne benefity</p>
                    </div>
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon offer__tile-icon--two"></div>
                        <p class="offer__tile-text">Dajemy Ci świadomość i&nbsp;wiedzę o ich wykorzystaniu</p>
                    </div>
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon offer__tile-icon--three"></div>
                        <p class="offer__tile-text">Dobieramy nowe i&nbsp;zawsze aktualne benefity</p>
                    </div>
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon offer__tile-icon--four"></div>
                        <p class="offer__tile-text">Pomagamy utrzymać pracowników w&nbsp;firmie</p>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>
