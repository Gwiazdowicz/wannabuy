<section class="clients">
        <div class="clients__border borders">
            <div class="clients__container container">
                <div class="clients__title-box">
                    <h2 class="clients__title a-title-two a-title-two--white"> Nasi klienci nie kryją zadowolenia </h2>
                </div>
                <div id="slideshow10">
                    <div class="slick slider-three">
                    <div class="clients__tile">
                            <div class="clients__tile-block">
                                <div class="clients__symbol"></div>
                                <p class="clients__text">Dzięki Wannabuy wydatki na rekrutację spadły o 30%! Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna.</p>
                                <p class="clients__name"><b>Marek</b>, HR Manager</p>
                                <div class="clients__logo clients__logo--one"></div>
                            </div> 
                        </div>
                       <div class="clients__tile">
                            <div class="clients__tile-block">
                                <div class="clients__symbol"></div>
                                <p class="clients__text">Dzięki Wannabuy wydatki na rekrutację spadły o 30%! Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna.</p>
                                <p class="clients__name"><b>Marek</b>, HR Manager</p>
                                <div class="clients__logo clients__logo--two"></div>
                            </div> 
                        </div>
                        <div class="clients__tile">
                            <div class="clients__tile-block">
                                <div class="clients__symbol"></div>
                                <p class="clients__text">Dzięki Wannabuy wydatki na rekrutację spadły o 30%! Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna.</p>
                                <p class="clients__name"><b>Marek</b>, HR Manager</p>
                                <div class="clients__logo clients__logo--one"></div>
                            </div> 
                        </div>
                    </div>
                </div>
               
            </div>
        </div>    
</section>
