<section class="companys">
    <div class="companys__border border">
        <div class="companys__container">
           <div id="slideshow3">
                <div class="slick slider-3">
                    <div  class="box"><div class="companys__icon"></div></div>
                    <div class="box"><div class="companys__icon companys__icon--one"></div></div>
                    <div  class="box"><div class="companys__icon"></div></div>
                    <div  class="box"><div class="companys__icon companys__icon--two"></div></div>
                    <div  class="box"><div class="companys__icon"></div></div>
                    <div  class="box"><div class="companys__icon companys__icon--three"></div></div>
                    <div  class="box"><div class="companys__icon"></div></div>
                    <div  class="box"><div class="companys__icon companys__icon--four"></div></div>
                    <div  class="box"><div class="companys__icon"></div></div>
                    <div  class="box"><div class="companys__icon companys__icon--five"></div></div>  
                </div>
            </div>
        </div>
    </div>
</section>
