<section class="problems">
        <div class="problems__border borders">
            <div class="problems__container container">
                <h2 class="problems__title a-title-two a-title--white --observe appear-up">Pewnie borykasz się z&nbsp;problemami HR,</h2>
                <p class="problems__subtitle --observe appear-up">przeszliśmy już te ścieżkę wiele razy z naszymi klientami</p>
                <p class="problems__box-one a-article a-article--white --observe appear-up"> W WannaBuy rozumiemy, że ludzie to jeden z ważniejszych składników każdej firmy. Robimy wszystko co tylko możliwe by pomóc Ci zatrzymać Twoich wyszkolonych i&nbsp;lojalnych pracowników w&nbsp;Twoim biznesie. Nie skupiamy się tylko na benefitach, nie chodzi nam tylko o&nbsp;to byś wydał pieniądze na przedpłaconą kartę, którą ktoś wrzuci do szuflady i&nbsp;nigdy nie wykorzysta - marnując tym samym Twoje ciężko zarobione pieniądze. Poświęcamy czas by szczerze zrozumieć Twój biznes i zasady pracy z&nbsp;ludźmi oraz&nbsp;analizujemy je pod każdym kątem wraz z&nbsp;osobami, które prowadzą Twój biznes. W&nbsp;następnej kolejności pracujemy z&nbsp;Tobą by dostarczyć Ci strategię, która pracuje dla Ciebie i&nbsp;Twojej firmy zaspokajając potrzeby i&nbsp;angażując Twoich ludzi. To nie wszystko - upewniamy się również, że wdrożony przez nas system działa analizując liczby i&nbsp;statystyki kadrowe i&nbsp;to czy rozwiązanie przynosi Ci realne korzyści. Mówiąc wprost, mniej zgadywania, mniej domysłów - po prostu miarodajne wyniki.</p>
            </div>
        </div>    
</section>
