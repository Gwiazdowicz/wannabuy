<section class="trends">
    <div class="trends__borders">
        <div class="trends__container">
            <div class="trends__content">
                <div class="trends__col-one u-pr">
                    
                    <div class="trends__book-wrapper u-pr">
                        <div class="trends__book-main --observe appear-up">                          
                        </div>
                        <img class="trends__arrow-1" src="<?= IMG . 'book-arrow-1.png' ?>" alt="">
                        <img class="trends__arrow-2" src="<?= IMG . 'book-arrow-2.png' ?>" alt="">
                        <img class="trends__arrow-3" src="<?= IMG . 'book-arrow-3.png' ?>" alt="">
                    </div>
                </div>
                <div class="trends__col-two">
                    <div class="trends__text-box">
                        <h2 class="trends__title --observe appear-up">5 trendów w&nbsp;HR na 2020</h2>
                        <p class="trends__subtitle --observe appear-up">Dlaczego pieniądze nie są już wystarczającą motywacją i co z tym zrobić?</p>
                        <p class="trends__article a-article --observe appear-up">Dziennie pomagamy naszym klientom oszczędzać pieniądze na rekrutacji i&nbsp;zatrzymywać lojalnych pracowników w&nbsp;firmie - wiemy jak to robić, zebraliśmy najważniejsze wskazówki w&nbsp;tym raporcie. Pobierz koniecznie zanim go stąd zdejmiemy albo dopadnie go Twoja konkurencja...</p>
                    </div>
                    <div class="trends__form-box --observe appear-up">
                        <form>
                            <label for="male">Wypełnij formularz</label>
                            <input placeholder="Wpisz imię" type="text" name="firstname">
                            <input placeholder="Wpisz nr telefonu" type="text" name="lastname">
                            <input placeholder="Wpisz email" type="text" name="firstname">
                            <input placeholder="Wpisz nazwę firmy" type="text" name="lastname">
                            <button class="trends__btn a-btn">Odbierz darmowy raport</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
