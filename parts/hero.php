<section class="hero --observe">
    <div class="hero__background">
        <div class="hero__content">
            <div class="hero__border borders">
                    <div class="hero__top-box">
                        <div class="hero__contact-box">
                            <div class="hero__phone"></div>
                            <a href="tel:48223506387" class="hero__contact-tel">+48 22 350 63 87</a>
                            <a href='http://localhost:3000/wannabuy-strona-firmowa/subpage.php' class="hero__contact-btn">
                                <p  class="hero__contact-btn-text">Rozpocznij</p>
                            </a>
                        </div>
                        <div class="hero__logo"></div>
                    </div>
                    <div class="hero__container container">
                    <div class="hero__bottom-box">
                        <div class="hero__text-box">
                            <h1 class="hero__title a-title"> Zatrzymaj pracowników na&nbsp;dłużej, zapomnij o&nbsp;staroświeckich benefitach</h1>
                            <div class="hero__lead-wrapper u-pr">
                                <div class="hero__arrow-left"></div>
                                <p class="hero__article a-subtitle">Zaledwie 27% pracowników korzysta z&nbsp;tradycyjnych benefitów takich jak opieka medyczna czy karta sportowa. Pokażemy Ci jak&nbsp;można zaangażować resztę!</p>
                                <div class="hero__arrow-right"></div>
                            </div>
                            <div class="hero__form-box">
                                <form>
                                    <input placeholder="Wpisz imię" type="text" name="firstname">
                                    <input placeholder="Wpisz email" type="text" name="lastname">
                                    <div class="hero__btn a-btn">Wyślij mi 5 trendów HR na&nbsp;2020&nbsp;rok</div>
                                </form> 
                            </div>
                            <div class="hero__mouse"></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>   
    </div>
</section>
