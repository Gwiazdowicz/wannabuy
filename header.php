<?php 
    const IMG = 'assets/img/';
?>

<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <!-- Check if JS is enabled, remove html "no-js" class    -->
    <script>(function (html) {
        html.className = html.className.replace(/\bno-js\b/, 'js')
      })(document.documentElement);</script>
    
    <script src="./critical.min.js" async></script>

    <?php
        // Add dynamic body body class based on current script name
        $body_class = null;
        if ( isset( $_SERVER['SCRIPT_NAME'] ) ) {
            $script_name = explode( '/', $_SERVER['SCRIPT_NAME'] );
            $filename    = $script_name[ count( $script_name ) - 1 ];
            $filename    = explode( '.', $filename )[0];
            $body_class  = $filename;
        }
    ?>

    <!-- external styles here -->
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:400,500,600,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <!-- /external styles here -->

    <?php
        if ( file_exists( __DIR__ . '/assets/css/style.min.css' ) ) {
            echo '<link rel="stylesheet" href="assets/css/style.min.css">';
        }
        if ( file_exists( __DIR__ . '/assets/css/style.css' ) ) {
            echo '<link rel="stylesheet" href="assets/css/style.css">';
        }
    ?>
    
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">        
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <?php
        if ( file_exists( __DIR__ . '/assets/js/main.min.js' ) ) {
            echo '<script src="assets/js/main.min.js" defer></script>';
        }
        if ( file_exists( __DIR__ . '/assets/js/main.js' ) ) {
            echo '<script src="assets/js/main.js" defer></script>';
        }
    ?>

</head>
<body class="<?= $body_class ?>">
<!--[if lt IE 11]>
<p>Używasz bardzo starej wersji przeglądarki Internet Explorer. Z&nbsp;tego też powodu ta&nbsp;strona najprawdopodobniej nie&nbsp;działa prawidłowo oraz narażasz się na potencjalne ryzyko związane z bezpieczeństwem.
  <a href="https://www.microsoft.com/pl-pl/download/Internet-Explorer-11-for-Windows-7-details.aspx" rel="noopener noreferrer">Wejdź tutaj, aby ją zaktualizować.</a>
</p>
<![endif]-->
<main>







