<section class="footer">
    <div class="footer__border borders">
        <div class="footer__container container">
            <div class="footer__top-block">
                <div class="footer__box">
                    <div class="footer__image"></div>
                </div>
                <div class="footer__box">
                    <p class="footer__title">Nr telefonu </p>
                    
                    <a href="tel:48223506387" class="footer__subtitle footer__link">+48 22 350 63 87</a>
                </div>
                <div class="footer__box">
                    <p class="footer__title">Email</p>
                    <a href="mailto:kontakt@wannaubuy.pl" class="footer__subtitle footer__link">kontakt@wannaubuy.pl</a>
                </div>
                <div class="footer__box">
                    <div class="footer__logo-box">
                        <a href="#!" target="_blank" rel="noopener noreferrer" class="footer__logo footer__logo--id"></a>
                        <a href="#!" target="_blank" rel="noopener noreferrer" class="footer__logo footer__logo--fb"></a>
                    </div>
                    <div class="footer__copy">2020 © Made with passion by Time4</div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom-block">
        <a href="tel:48123506387"  class="footer__num">+48 12 350 63 87</a>
        <div class="footer__btn">
            <p>Rozpocznij</p>
        </div>
    </div>
</section>
</main>
</body>
</html>
