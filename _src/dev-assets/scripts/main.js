console.log('main.js imported');

$('#slideshow10 .slick').slick({
  slidesToShow: 1,
  dots: false,
  arrows: false,
  autoplay: true,
  mobileFirst: true,
  centerMode: true,
  centerPadding: '30px',
  responsive: [
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 1200,
      settings: 'unslick'
    }
  ]
});

$(window).on('load', function() {
  const $sliders = $('.js-init-slider');

  const sliderOptions = {
    logosOne: {
      slidesToShow: 2,
      variableWidth: true,
      dots: false,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 1000,
      mobileFirst: true,
      infinite: true,
      responsive: [
        {
          breakpoint: 650,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        }
      ]
    }
  };

  $sliders.each(function() {
    const sliderName = $(this).attr('data-slider');
    $(this).slick(sliderOptions[sliderName]);
  });
});

if ($('body').hasClass('subpage')) {
  const fullTime = 420; // 100%
  const initTime = 251; // in seconds

  let leftTime = initTime;
  let progress = (leftTime / fullTime) * 100; // %

  const $progressBar = $('.js-progress');
  const $timeDisplay = $('.js-time-display');

  const getTimeInMinutes = timeInSeconds => {
    const minutes = Math.floor(timeInSeconds / 60);
    let seconds = timeInSeconds - minutes * 60;

    if (seconds < 10) {
      seconds = '0' + seconds;
    }

    $timeDisplay.html(`${minutes}:${seconds}`);
  };

  const calculateAndSetProgress = () => {
    progress = (leftTime / fullTime) * 100;
    $progressBar.css('width', `${progress}%`);
  };

  const tick = () => {
    console.log('tick');
    leftTime -= 1;
    calculateAndSetProgress();
    getTimeInMinutes(leftTime);
  };

  calculateAndSetProgress();
  getTimeInMinutes(leftTime);
  setInterval(tick, 1000);
}
