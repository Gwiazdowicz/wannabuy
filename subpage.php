<?php

require_once(__DIR__.'/header.php');

?>

<section class="hero hero__sub">
    <div class="hero__background">
        <div class="hero__content">
            <div class="hero__border borders">
                    <div class="hero__top-box hero__top-box-sub">
                        <div class="hero__contact-box">
                            <div class="hero__phone"></div>
                            <a href="tel:48223506387" class="hero__contact-tel">+48 22 350 63 87</a>
                            <a href="!#" class="hero__contact-btn">
                                <p  class="hero__contact-btn-text">Rozpocznij</p>
                            </a>
                        </div>
                        <div class="hero__logo"></div>
                    </div>
                    <div class="hero__progress-box">
                        <p class="hero__progress-text">Pospiesz się!<span class="hero__progress-text--red"> Liczba konsultacji ograniczona</span></p>
                        <div class="hero__bar">
                            <div class="hero__progress-bar">
                                <div class="hero__progress-level js-progress"></div>
                            </div>
                            <div class="hero__progress-timer js-time-display">                                
                            </div>
                        </div>
                    </div>
                    <div class="hero__container container">
                    <div class="hero__bottom-box">
                        <div class="hero__text-box">
                            <h1 class="hero__title a-title">Skorzystaj z&nbsp;darmowej 30 minutowej konsultacji dla działu&nbsp;hr</h1>
                            <div class="hero__lead-wrapper u-pr">
                                <div class="hero__arrow-left-sub"></div>
                                <p class="hero__article hero__article-sub a-subtitle"><b>Odbierz w&nbsp;100% darmową i&nbsp;bez zobowiązań</b> 30 minutową konsultację dla działu HR (o wartości 1000 zł), która pomoże Ci zatrzymać pracowników w&nbsp;Twojej firmie. Spiesz się, dostępność naszych analityków z każdą chwilą maleje!</p>
                                <div class="hero__arrow-right-sub"></div>
                            </div>
                            <div class="hero__form-box-sub">
                                <form>
                                    <input placeholder="Wpisz imię" type="text" name="firstname">
                                    <input placeholder="Wpisz email" type="text" name="lastname">
                                    <div class="hero__btn-sub a-btn">Odbierz darmową 30-minutową konsultację hr<br><span>Ilość miejsc ograniczona</span></div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>   
    </div>
</section>

<div class="partners l-borders partners__sub">
    <div class="l-container u-pr">
        <h2 class="partners__title">Widziani w</h2>
        <div data-slider="logosOne" class="js-init-slider partners__slider">
            <div class="partners__slider-slide">
                <img class="partners__slider-item" src="<?= IMG . 'slider-1.1.png' ?>" alt="">
            </div>
            <div class="partners__slider-slide">
                <img class="partners__slider-item" src="<?= IMG . 'slider-1.2.png' ?>" alt="">
            </div>
            <div class="partners__slider-slide">
                <img class="partners__slider-item" src="<?= IMG . 'slider-1.3.png' ?>" alt="">
            </div>
            <div class="partners__slider-slide">
                <img class="partners__slider-item" src="<?= IMG . 'slider-1.4.png' ?>" alt="">
            </div>
        </div>        
    </div>
</div>

<section class="footer">
    <div class="footer__border borders">
        <div class="footer__container container">
            <div class="footer__top-block">
                <div class="footer__box">
                    <div class="footer__image"></div>
                </div>
                <div class="footer__box">
                    <p class="footer__title">Nr telefonu </p>
                    <a href="tel:48224300330" class="footer__subtitle footer__link">+48 22 430 03 30</a>
                </div>
                <div class="footer__box">
                    <p class="footer__title">Email</p>
                    <a href="mailto:kontakt@wannaubuy.pl" class="footer__subtitle footer__link">kontakt@wannaubuy.pl</a>
                </div>
                <div class="footer__box">
                    <div class="footer__logo-box">
                        <a href="#!" class="footer__logo footer__logo--tweet"></a>
                        <a href="#!" class="footer__logo footer__logo--fb"></a>
                    </div>
                    <div class="footer__copy">2020 © Made with passion by Time4</div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom-block">
        <div class="footer__num">+48 22 430 03 30</div>
        <div class="footer__btn">
            <p>Rozpocznij</p>
        </div>
    </div>
</section>
</main>
</body>
</html>
